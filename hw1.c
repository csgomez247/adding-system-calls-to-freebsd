#include <sys/types.h>
#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/lock.h>
#include <sys/mutex.h>

/*
	Arguemnt Strucutres
*/

struct setProcessTickets_args 
{
    int pid;
    int tickets;
};

struct getProcessTickets_args
{
    int pid;
};

struct setSocialInfo_args
{
	int pid;
	u_int64_t social_info;
};

struct getSocialInfo_args
{
	int pid;
};

struct setLotteryMode_args
{
	int mode;
};

/*
	System Calls
*/

static int
setProcessTickets(struct thread *td, struct setProcessTickets_args *arg)
{
    struct proc * procPtr = pfind(arg->pid);
    if(procPtr != 0)
    {
    	PROC_UNLOCK(procPtr);
		procPtr->tickets = arg->tickets;
		td->td_retval[0] = procPtr->tickets;
    }//if
    else
		td->td_retval[0] = -1;
	return 0;
}//setProcessTickets

static int getProcessTickets(struct thread *td, struct getProcessTickets_args *arg)
{
    struct proc * procPtr = pfind(arg->pid);
    if(procPtr != 0)
    {
    	PROC_UNLOCK(procPtr);
		td->td_retval[0] = procPtr->tickets;
    }//if
    else
		td->td_retval[0] = -1;
	return 0;
}//getProcessTickets

static int setSocialInfo(struct thread *td, struct setSocialInfo_args * arg)
{
	struct proc * procPtr = pfind(arg->pid);
	if(procPtr != 0)
	{
		PROC_UNLOCK(procPtr);
		procPtr->social_info = arg->social_info; 
		td->td_retval[0] = procPtr->social_info;
	}//if
	else
		td->td_retval[0] = -1;
	return 0;
}//setSocialInfo

static int getSocialInfo(struct thread *td, struct getSocialInfo_args * arg)
{
	struct proc * procPtr = pfind(arg->pid);
	if(procPtr != 0)
	{
		PROC_UNLOCK(procPtr);
		td->td_retval[0] = procPtr->social_info;
	}//if
	else
		td->td_retval[0] = -1;
	return 0;
}//getSocialInfo

static int setLotteryMode(struct thread *td, struct setLotteryMode_args * arg)
{
	lottery_mode = arg->mode;	
	return 0;
}//setLotteryMod

static int getLotteryMode(struct thread *td, void * arg)
{
	td->td_retval[0] = lottery_mode;
	return 0;
}//getLotteryMode

static struct sysent setProcessTickets_sysent = { 2, setProcessTickets };
static struct sysent getProcessTickets_sysent = { 1, getProcessTickets };
static struct sysent setSocialInfo_sysent = {2, setSocialInfo};
static struct sysent getSocialInfo_sysent = {1, getSocialInfo};
static struct sysent setLotteryMode_sysent = {1, setLotteryMode};
static struct sysent getLotteryMode_sysent = {0, getLotteryMode};

static int offset_setProcessTickets = NO_SYSCALL;
static int offset_getProcessTickets = NO_SYSCALL;
static int offset_setSocialInfo = NO_SYSCALL;
static int offset_getSocialInfo = NO_SYSCALL;
static int offset_setLotteryMode = NO_SYSCALL;
static int offset_getLotteryMode = NO_SYSCALL;

static int
load_setProcessTickets(struct module *module, int cmd, void *arg)
{
    int error = 0;
    
    switch (cmd)
    {
        case MOD_LOAD:
            printf("syscall setProcessTickets loaded at %d\n", offset_setProcessTickets);
            break;
        case MOD_UNLOAD:
            printf("syscall setProcessTickets unloaded at %d\n", offset_setProcessTickets);
            break;
        default:
            error = EINVAL;
            break;
    }
  return error;
}

static int
load_getProcessTickets(struct module *module, int cmd, void *arg)
{
    int error = 0;
    
    switch (cmd)
    {
        case MOD_LOAD:
            printf("syscall getProcessTickets loaded at %d\n", offset_getProcessTickets);
            break;
        case MOD_UNLOAD:
            printf("syscall getProcessTickets unloaded at %d\n", offset_getProcessTickets);
            break;
        default:
            error = EINVAL;
            break;
    }
  return error;
}

static int
load_setSocialInfo(struct module *module, int cmd, void *arg)
{
    int error = 0;
    
    switch (cmd)
    {
        case MOD_LOAD:
            printf("syscall setSocialInfo loaded at %d\n", offset_setSocialInfo);
            break;
        case MOD_UNLOAD:
            printf("syscall setSocialInfo unloaded at %d\n", offset_setSocialInfo);
            break;
        default:
            error = EINVAL;
            break;
    }
  return error;
}

static int
load_getSocialInfo(struct module *module, int cmd, void *arg)
{
    int error = 0;
    
    switch (cmd)
    {
        case MOD_LOAD:
            printf("syscall getSocialInfo loaded at %d\n", offset_getSocialInfo);
            break;
        case MOD_UNLOAD:
            printf("syscall getSocialInfo unloaded at %d\n", offset_getSocialInfo);
            break;
        default:
            error = EINVAL;
            break;
    }
  return error;
}

static int
load_setLotteryMode(struct module *module, int cmd, void *arg)
{
    int error = 0;
    
    switch (cmd)
    {
        case MOD_LOAD:
            printf("syscall setLotteryMode loaded at %d\n", offset_setLotteryMode);
            break;
        case MOD_UNLOAD:
            printf("syscall setLotteryMode unloaded at %d\n", offset_setLotteryMode);
            break;
        default:
            error = EINVAL;
            break;
    }
  return error;
}

static int
load_getLotteryMode(struct module *module, int cmd, void *arg)
{
    int error = 0;
    
    switch (cmd)
    {
        case MOD_LOAD:
            printf("syscall getLotteryMode loaded at %d\n", offset_getLotteryMode);
            break;
        case MOD_UNLOAD:
            printf("syscall getLotteryMode unloaded at %d\n", offset_getLotteryMode);
            break;
        default:
            error = EINVAL;
            break;
    }
  return error;
}

SYSCALL_MODULE(setProcessTickets, &offset_setProcessTickets, &setProcessTickets_sysent, load_setProcessTickets, NULL);
SYSCALL_MODULE(getProcessTickets, &offset_getProcessTickets, &getProcessTickets_sysent, load_getProcessTickets, NULL);
SYSCALL_MODULE(setSocialInfo, &offset_setSocialInfo, &setSocialInfo_sysent, load_setSocialInfo, NULL);
SYSCALL_MODULE(getSocialInfo, &offset_getSocialInfo, &getSocialInfo_sysent, load_getSocialInfo, NULL);
SYSCALL_MODULE(setLotteryMode, &offset_setLotteryMode, &setLotteryMode_sysent, load_setLotteryMode, NULL);
SYSCALL_MODULE(getLotteryMode, &offset_getLotteryMode, &getLotteryMode_sysent, load_getLotteryMode, NULL);
